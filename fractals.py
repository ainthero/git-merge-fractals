import numpy

def in_circle(z, radius):
  return z.real ** 2 + z.imag ** 2 < radius ** 2

def fractal_eta(func, z, limit, radius=2):
    cnt = 0
    while in_circle(z, radius):
        cnt += 1
        if cnt >= limit:
            break
        z = func(z)
    return cnt

def cqp(c):
  """ Complex quadratic polynomial, function used for Mandelbrot fractal """
  return lambda z: z ** 2 + c


def translate_coordinates(size, center, zoom):
    width, height = size
    cx, cy = center
    side = max(width, height)
    sidem1 = side - 1
    delta_x = (side - width) / 2  # Centralize
    delta_y = (side - height) / 2

    def func(row, col):
        y = (2 * (height - row + delta_y) / sidem1 - 1) / zoom + cy
        x = (2 * (col + delta_x) / sidem1 - 1) / zoom + cx
        return x, y

    return func

def get_model(model, depth, c):
  """ Returns the fractal model function """
  if model == "julia":
    func = cqp(c)
    return lambda x, y: fractal_eta(func, x + y * 1j, depth)
  elif model == "mandelbrot":
    return lambda x, y: fractal_eta(cqp(x + y * 1j), 0j, depth)
  elif model == "my_own":
    return lambda x, y: numpy.sin(x**2 + y)

  raise ValueError("Fractal not found")


def render_fractal(func, size):
    img = numpy.empty(size)
    width, height = size
    for row in range(width):
        for col in range(height):
            img[row, col] = func(row, col)

    return img


def make_fractal(model='julia', c=1.0j, depth=256, size=(512, 512),
                 zoom=1.0, center=(0.0,0.0)):
    func = get_model(model, depth, c)
    t = translate_coordinates(size, center, zoom)

    pixel_by_coords = lambda row, col: func(*t(row, col))
    return render_fractal(pixel_by_coords, size)